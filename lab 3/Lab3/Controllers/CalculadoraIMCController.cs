﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lab3.Models;


namespace Lab3.Controllers
{
    public class CalculadoraIMCController : Controller
    {
        public PersonaModel[] vectorPersona;
        public double[] imcS;
        public double[] estaV;
        public double[] pesoV;
        // GET: CalculadoraIMC
        public ActionResult ResultadoIMC()
        {
            PersonaModel persona = new PersonaModel(1, "Cristiano Ronaldo", 84.0, 1.87);
            double IMC = persona.Peso / (persona.Estatura * persona.Estatura);
            ViewBag.IMC = IMC;
            ViewBag.persona = persona;
            return View();
        }
        public ActionResult personaAleatoria()
        {
            double IMC = 0.0;
            vectorPersona = new PersonaModel[30];
            imcS = new double[30];
            Random r = new Random();
            double pesoV = 0.0; //for doubles
            Random r2 = new Random();
            double estaV = 0.0;
            for (int i = 0; i < 30; i++)
            {
                estaV = r2.NextDouble() * (2.0 - 1.0) + 1.0;
                pesoV = r.NextDouble() * (150.0 - 20.0) + 20.0;
                vectorPersona[i] = new PersonaModel(i, "Sin nombre", pesoV, estaV);
                IMC = vectorPersona[i].Peso / (vectorPersona[i].Estatura * vectorPersona[i].Estatura);
                imcS[i] = IMC;
            }
            ViewBag.IMC = imcS;
            ViewBag.vectorPersona = vectorPersona;
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}